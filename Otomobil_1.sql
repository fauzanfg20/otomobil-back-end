-- DROP DATABASE Otomobil;
CREATE DATABASE Otomobil;
USE Otomobil;

USE db_test;
DROP DATABASE Otomobil;

-- tables
CREATE TABLE tbl_user(
    user_id INT PRIMARY KEY IDENTITY(1,1),
    [name] VARCHAR(60),
    email VARCHAR(255),
    confirmed_email BIT, -- tidak ada boolean di SQL server
    password_hash VARBINARY(256),
    salt_hash VARBINARY(256),
    can_be_admin BIT,
    [role] VARCHAR(6) DEFAULT 'user'
);

CREATE TABLE tbl_token(
    token_id INT IDENTITY(1,1) PRIMARY KEY,
    fk_user_id INT NOT NULL,
    otp VARCHAR(255) UNIQUE,
    register_token VARCHAR(255) UNIQUE,
    reset_token VARCHAR(255) UNIQUE,
    expired_date DATETIME,
    email VARCHAR(255) UNIQUE,
    CONSTRAINT token_fk_user_id FOREIGN KEY (fk_user_id) 
    REFERENCES tbl_user(user_id) ON DELETE CASCADE ON UPDATE CASCADE
);

CREATE TABLE tbl_invoice(
    invoice_id INT PRIMARY KEY IDENTITY(1,1) NOT NULL,
    fk_user_id INT NOT NULL,
    user_name VARCHAR(60) NOT NULL,
    user_email VARCHAR(255) NOT NULL,
    invoice_date DATE NOT NULL, 
    total_price INT NOT NULL,
    payment_method VARCHAR(20)
);

CREATE TABLE tbl_car_type(
    car_type_id INT PRIMARY KEY IDENTITY(1,1) NOT NULL,
    car_type VARCHAR(30),
    thumbnail VARCHAR(max),
    banner_image VARCHAR(max),
    [car_type_description] VARCHAR(max),
);

CREATE TABLE tbl_course(
    course_id INT PRIMARY KEY IDENTITY(1,1) NOT NULL,
    [course_name] VARCHAR(60),
    fk_car_type_id INT NOT NULL,
    [course_description] VARCHAR(max),
    [image] VARCHAR(max), 
    price INT,
    CONSTRAINT course_fk_car_type_id FOREIGN KEY (fk_car_type_id) 
    REFERENCES tbl_car_type(car_type_id)
    ON DELETE CASCADE
    ON UPDATE CASCADE
);

CREATE TABLE tbl_invoice_detail(
    fk_invoice_id INT NOT NULL,
    cart_id INT NOT NULL,
    course_id INT NOT NULL,
    course_name VARCHAR(60) NOT NULL,
    car_type VARCHAR(30) NOT NULL,
    schedule DATE,
    price INT,
    [image] VARCHAR(max),
    CONSTRAINT invoice_fk_invoice_id FOREIGN KEY (fk_invoice_id) 
    REFERENCES tbl_invoice(invoice_id) 
    ON DELETE CASCADE ON UPDATE CASCADE
);

CREATE TABLE tbl_cart (
    cart_id INT PRIMARY KEY IDENTITY(1,1) NOT NULL,
    fk_user_id INT NOT NULL,
    fk_course_id INT NOT NULL,
    [schedule] DATE,
    CONSTRAINT cart_fk_user_id FOREIGN KEY (fk_user_id) 
    REFERENCES tbl_user(user_id) ON DELETE CASCADE ON UPDATE CASCADE,
    CONSTRAINT cart_fk_course_id FOREIGN KEY (fk_course_id) 
    REFERENCES tbl_course(course_id) ON DELETE CASCADE ON UPDATE CASCADE
);

CREATE TABLE payment_method(
    id INT PRIMARY KEY IDENTITY(1,1) NOT NULL,
    name varchar(20),
    icon VARCHAR(max),
    active BIT,
);
-- end of tables


INSERT INTO tbl_car_type(car_type,car_type_description,thumbnail,banner_image) VALUES
 ('Electric','Lorem ipsum, dolor sit amet consectetur adipisicing elit. Aperiam velit ex, voluptate, eaque provident libero perferendis ratione eos nemo hic atque, et debitis laboriosam non est ea consequuntur numquam id?','https://iili.io/H1UYTPf.png','https://freeimage.host/i/H1bVeNR')
,('Hatcback','Lorem ipsum, dolor sit amet consectetur adipisicing elit. Aperiam velit ex, voluptate, eaque provident libero perferendis ratione eos nemo hic atque, et debitis laboriosam non est ea consequuntur numquam id?','https://iili.io/H1UYAF4.png','https://freeimage.host/i/H1bVeNR')
,('LCGC','Lorem ipsum, dolor sit amet consectetur adipisicing elit. Aperiam velit ex, voluptate, eaque provident libero perferendis ratione eos nemo hic atque, et debitis laboriosam non est ea consequuntur numquam id?','https://iili.io/H1UYIMG.png','https://iili.io/H1bVeNR.png')
,('MPV','Lorem ipsum, dolor sit amet consectetur adipisicing elit. Aperiam velit ex, voluptate, eaque provident libero perferendis ratione eos nemo hic atque, et debitis laboriosam non est ea consequuntur numquam id?','https://iili.io/H1UYzns.png','https://iili.io/H1bVeNR.png')
,('Offroad','Lorem ipsum, dolor sit amet consectetur adipisicing elit. Aperiam velit ex, voluptate, eaque provident libero perferendis ratione eos nemo hic atque, et debitis laboriosam non est ea consequuntur numquam id?','https://iili.io/H1UclEb.png','https://iili.io/H1bVeNR.png')
,('Sedan','Lorem ipsum, dolor sit amet consectetur adipisicing elit. Aperiam velit ex, voluptate, eaque provident libero perferendis ratione eos nemo hic atque, et debitis laboriosam non est ea consequuntur numquam id?','https://iili.io/H1UYRcl.png','https://iili.io/H1bVeNR.png')
,('SUV','Lorem ipsum, dolor sit amet consectetur adipisicing elit. Aperiam velit ex, voluptate, eaque provident libero perferendis ratione eos nemo hic atque, et debitis laboriosam non est ea consequuntur numquam id?','https://iili.io/H1UY5S2.png','https://iili.io/H1bVeNR.png')
,('Truck','Lorem ipsum, dolor sit amet consectetur adipisicing elit. Aperiam velit ex, voluptate, eaque provident libero perferendis ratione eos nemo hic atque, et debitis laboriosam non est ea consequuntur numquam id?','https://iili.io/H1UYY9S.png','https://iili.io/H1bVeNR.png');

INSERT INTO tbl_course (course_name,price,[image],fk_car_type_id, [course_description]) VALUES 
('Course SUV Kijang Innova',700000,'https://iili.io/H1St4K7.png',7,'Lorem ipsum, dolor sit amet consectetur adipisicing elit. Aperiam velit ex, voluptate, eaque provident libero perferendis ratione eos nemo hic atque, et debitis laboriosam non est ea consequuntur numquam id?')
, ('Course LCGC',700000,'https://iili.io/H1StUV2.png',7,'Lorem ipsum, dolor sit amet consectetur adipisicing elit. Aperiam velit ex, voluptate, eaque provident libero perferendis ratione eos nemo hic atque, et debitis laboriosam non est ea consequuntur numquam id?')
, ('Hyundai Paligase 2021',800000,'https://iili.io/H1StgPS.png',7,'Lorem ipsum, dolor sit amet consectetur adipisicing elit. Aperiam velit ex, voluptate, eaque provident libero perferendis ratione eos nemo hic atque, et debitis laboriosam non est ea consequuntur numquam id?')
, ('Course Mitsubishi Pajero',800000,'https://iili.io/H1StSol.png',7,'Lorem ipsum, dolor sit amet consectetur adipisicing elit. Aperiam velit ex, voluptate, eaque provident libero perferendis ratione eos nemo hic atque, et debitis laboriosam non est ea consequuntur numquam id?')
, ('Dump Truck for Mining Constructor',1200000,'https://iili.io/H1St6l9.png',8,'Lorem ipsum, dolor sit amet consectetur adipisicing elit. Aperiam velit ex, voluptate, eaque provident libero perferendis ratione eos nemo hic atque, et debitis laboriosam non est ea consequuntur numquam id?')
, ('Sedan Honda Civic',400000,'https://iili.io/H1StPSe.png',6,'Lorem ipsum, dolor sit amet consectetur adipisicing elit. Aperiam velit ex, voluptate, eaque provident libero perferendis ratione eos nemo hic atque, et debitis laboriosam non est ea consequuntur numquam id?')
, ('Course SUV Kijang Innova',700000,'https://iili.io/H1St4K7.png',7,'Lorem ipsum, dolor sit amet consectetur adipisicing elit. Aperiam velit ex, voluptate, eaque provident libero perferendis ratione eos nemo hic atque, et debitis laboriosam non est ea consequuntur numquam id?')
, ('Course LCGC',700000,'https://iili.io/H1StUV2.png',3,'Lorem ipsum, dolor sit amet consectetur adipisicing elit. Aperiam velit ex, voluptate, eaque provident libero perferendis ratione eos nemo hic atque, et debitis laboriosam non est ea consequuntur numquam id?')
, ('Hyundai Paligase 2021',800000,'https://iili.io/H1StgPS.png',7,'Lorem ipsum, dolor sit amet consectetur adipisicing elit. Aperiam velit ex, voluptate, eaque provident libero perferendis ratione eos nemo hic atque, et debitis laboriosam non est ea consequuntur numquam id?')
, ('Course Mitsubishi Pajero',800000,'https://iili.io/H1StSol.png',7,'Lorem ipsum, dolor sit amet consectetur adipisicing elit. Aperiam velit ex, voluptate, eaque provident libero perferendis ratione eos nemo hic atque, et debitis laboriosam non est ea consequuntur numquam id?')
, ('Dump Truck for Mining Constructor',1200000,'https://iili.io/H1St6l9.png',8,'Lorem ipsum, dolor sit amet consectetur adipisicing elit. Aperiam velit ex, voluptate, eaque provident libero perferendis ratione eos nemo hic atque, et debitis laboriosam non est ea consequuntur numquam id?')
, ('Course SUV Kijang Innova',700000,'https://iili.io/H1St4K7.png',7,'Lorem ipsum, dolor sit amet consectetur adipisicing elit. Aperiam velit ex, voluptate, eaque provident libero perferendis ratione eos nemo hic atque, et debitis laboriosam non est ea consequuntur numquam id?')
, ('Course LCGC',700000,'https://iili.io/H1StUV2.png',3,'Lorem ipsum, dolor sit amet consectetur adipisicing elit. Aperiam velit ex, voluptate, eaque provident libero perferendis ratione eos nemo hic atque, et debitis laboriosam non est ea consequuntur numquam id?')
, ('Hyundai Paligase 2021',800000,'https://iili.io/H1StgPS.png',7,'Lorem ipsum, dolor sit amet consectetur adipisicing elit. Aperiam velit ex, voluptate, eaque provident libero perferendis ratione eos nemo hic atque, et debitis laboriosam non est ea consequuntur numquam id?')
, ('Course Mitsubishi Pajero',800000,'https://iili.io/H1StSol.png',7,'Lorem ipsum, dolor sit amet consectetur adipisicing elit. Aperiam velit ex, voluptate, eaque provident libero perferendis ratione eos nemo hic atque, et debitis laboriosam non est ea consequuntur numquam id?')
, ('Dump Truck for Mining Constructor',1200000,'https://iili.io/H1St6l9.png',8,'Lorem ipsum, dolor sit amet consectetur adipisicing elit. Aperiam velit ex, voluptate, eaque provident libero perferendis ratione eos nemo hic atque, et debitis laboriosam non est ea consequuntur numquam id?')

INSERT INTO payment_method (name, icon, active) VALUES
	('Gopay', 'https://i.ibb.co/HpkKDZ1/icon-gopay.png', 'true'),
	('OVO', 'https://i.ibb.co/vLDSd7D/icon-ovo.png', 'true'),
	('Dana', 'https://i.ibb.co/3pYBsr8/icon-dana.png', 'true'),
	('Mandiri', 'https://i.ibb.co/7ptzK3L/icon-mandiri.png', 'true'),
	('BCA', 'https://i.ibb.co/tzxsmzB/icon-bca.png', 'true'),
	('BNI', 'https://i.ibb.co/ScN1xPL/icon-bni.png', 'true');