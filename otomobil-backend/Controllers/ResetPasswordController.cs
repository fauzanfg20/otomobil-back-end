﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

using System.Data;
using System.Data.SqlClient;
//using System.Security.Cryptography; // ini untuk hash
using System.Text;

using otomobil_backend.Models;
using otomobil_backend.Logics;
using System.Security.Claims;

namespace otomobil_backend.Controllers
{
    [Route("api/")]
    [ApiController]
    public class ResetPasswordController : Controller
    {
            #region reset password
            [HttpPost]
            [Route("new-password/{reset_token}")]
            public ActionResult UserActivationByToken(string? reset_token, [FromBody] UserPasswordReset new_password )
            {
                try
                {
                    // validasi: cek user belum ada di database
                    string query = "SELECT TOP 1 email FROM tbl_token WHERE reset_token=@token";
                    SqlParameter[] sqlParams = new SqlParameter[]
                    {
                    new SqlParameter("@token", SqlDbType.VarChar) { Value = reset_token ?? "" },
                    };

                    object emailObj = CRUD.ExecuteScalar(query, sqlParams);
                    string email = emailObj == DBNull.Value ? "" : (string)emailObj;

                    if (String.IsNullOrEmpty(email))
                    {
                        throw new Exception("Reset Token Denied");
                    }
                    // create hash
                    byte[] passwordHash = Array.Empty<byte>();
                    byte[] passwordSalt = Array.Empty<byte>();
                    
                    CryptoLogic.GenerateHash(new_password.password, out passwordHash, out passwordSalt); // cara 2
                    
                    // activate the user
                    query = "UPDATE tbl_user SET password_hash = @password_hash, salt_hash = @password_salt WHERE email=@email;";
                    sqlParams = new SqlParameter[]
                    {
                    new SqlParameter("@email", SqlDbType.VarChar) { Value = email },
                    new SqlParameter("@password_hash", SqlDbType.VarBinary) { Value = passwordHash },
                    new SqlParameter("@password_salt", SqlDbType.VarBinary) { Value = passwordSalt },
                    };
                    CRUD.ExecuteNonQuery(query, sqlParams);

                    return Ok("Success");
                }
                catch (Exception ex)
                {
                    return BadRequest(ex.Message);
                }
            }
            #endregion
            #region reset password validate token
            [HttpGet]
            [Route("reset-password-validate-token/{reset_token}")]
            public ActionResult UserActivationByToken(string? reset_token )
            {
                try
                {
                    // validasi: cek user belum ada di database
                    string query = "SELECT TOP 1 email FROM tbl_token WHERE reset_token=@token";
                    SqlParameter[] sqlParams = new SqlParameter[]
                    {
                    new SqlParameter("@token", SqlDbType.VarChar) { Value = reset_token ?? "" },
                    };

                    object emailObj = CRUD.ExecuteScalar(query, sqlParams);
                    string email = emailObj == DBNull.Value ? "" : (string)emailObj;

                    if (String.IsNullOrEmpty(email))
                    {
                        throw new Exception("Reset Token Denied");
                    }

                    return Ok("token valid");
                }
                catch (Exception ex)
                {
                    return BadRequest(ex.Message);
                }
            }
            #endregion
    }
}
