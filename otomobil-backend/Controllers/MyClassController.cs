﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using otomobil_backend.Logics;
using otomobil_backend.Models;
using System.Data;

namespace otomobil_backend.Controllers
{
    [Route("api/")]
    [ApiController]
    public class MyClassController : Controller
    {
        [HttpGet]
        [Route("my-class")]
        [Authorize(Roles = "admin, user")]
        public ActionResult GetInvoices([FromQuery] string? user_id, string? course_id, string?schedule)
        {
            try
            {
                List<DetailInvoice> result = new();
                result = MyClassLogic.GetUserClasses(user_id, course_id, schedule);
                return Ok(result);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
    }
}
