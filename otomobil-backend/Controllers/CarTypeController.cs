﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using otomobil_backend.Logics;
using otomobil_backend.Models;
using System.Data;

namespace otomobil_backend.Controllers
{
    [Route("api/")]
    [ApiController]
    public class CarTypeController : ControllerBase
    {
        [HttpGet]
        [Route("car-type")]
        public ActionResult GetCarType([FromQuery] string? id)
        {
            try
            {
                List<CarType> result = new List<CarType>(); // initialisasi array kosong
                result = CarTypeLogic.GetCarType(id);
                return Ok(result);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        #region Insert Data
        [HttpPost]
        [Route("car-type")]
        [Authorize(Roles = "admin")]
        public ActionResult AddCarType([FromBody] CarTypeInsert body)
        {
            try
            {
                CarTypeLogic.AddCarType(body);
                return Ok("Success");
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
        #endregion

        #region Update Data
        [HttpPut]
        [Route("car-type/{id}")]
        [Authorize(Roles = "admin")]
        public ActionResult EditCarType([FromBody] CarTypeInsert body, [FromRoute] string id)
        {
            try
            {
                CarTypeLogic.EditCarType(body, id);
                return Ok("Success");
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
        #endregion

        #region Delete Data
        [HttpDelete]
        [Route("car-type/{id}")]
        [Authorize(Roles = "admin")]
        public ActionResult DeleteCarType([FromRoute] string id)
        {
            try
            {
                CarTypeLogic.DeleteCarType(id);
                return Ok("Success");
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
        #endregion
    }
}
