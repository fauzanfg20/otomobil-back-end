﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

using System.Data;
using System.Data.SqlClient;
//using System.Security.Cryptography; // ini untuk hash
using System.Text;

using otomobil_backend.Models;
using otomobil_backend.Logics;
using System.Security.Claims;

namespace otomobil_backend.Controllers
{
    
    [Route("api/")]
    [ApiController]
    public class RegisterController : ControllerBase
    {
        private string FrontUrl = "";
        public RegisterController(IConfiguration configuration)
        {
            FrontUrl = configuration["FrontUrl"];
        }
        #region registerAPI
        [HttpPost]
        [Route("register")]
        public ActionResult Register([FromBody] RegisterUser user)
        {
            using (SqlConnection con = CRUD.GenerateConnection())
            {
                con.Open();
                SqlTransaction trans = con.BeginTransaction();
                SqlCommand command = con.CreateCommand();
                command.Transaction = trans;
                try
                {
                    // validasi: data input user tidak ada yang kosong
                    if (String.IsNullOrEmpty(user.name))
                    {
                        throw new Exception("Name cannot empty");
                    }
                    if (String.IsNullOrEmpty(user.email))
                    {
                        throw new Exception("Email cannot empty");
                    }
                    if (String.IsNullOrEmpty(user.password))
                    {
                        throw new Exception("Password cannot empty");
                    }

                    // validasi: cek user belum ada di database
                    string query = "SELECT TOP 1 * FROM tbl_user WHERE email=@email";
                    SqlParameter[] sqlParams = new SqlParameter[]
                    {
                    new SqlParameter("@email", SqlDbType.VarChar) { Value = user.email ?? "" },
                    };
                    DataTable userTable = CRUD.ExecuteQuery(query, sqlParams);
                    if (userTable.Rows.Count > 0)
                    {
                        throw new Exception("User with email: " + user.email + " already exist");
                    }

                    // create hash
                    byte[] passwordHash = Array.Empty<byte>();
                    byte[] passwordSalt = Array.Empty<byte>();

                    //(passwordHash, passwordSalt) = CryptoLogic.GenerateHash(user.password); // cara 1
                    CryptoLogic.GenerateHash(user.password ?? "", out passwordHash, out passwordSalt); // cara 2

                    // insert key and hash to db
                    command.CommandText = "INSERT INTO tbl_user([name], email, confirmed_email, password_hash, salt_hash, can_be_admin, role) OUTPUT INSERTED.user_id VALUES (@name, @email, 0, @password_hash, @salt_hash, 1, 'user')";
                    sqlParams = new SqlParameter[]
                    {
                    new SqlParameter("@email", SqlDbType.VarChar) { Value = user.email ?? "" },
                    new SqlParameter("@name", SqlDbType.VarChar) { Value = user.name ?? "" },
                    new SqlParameter("@password_hash", SqlDbType.VarBinary) { Value = passwordHash },
                    new SqlParameter("@salt_hash", SqlDbType.VarBinary) { Value = passwordSalt }
                    };
                    foreach (SqlParameter sqlParam in sqlParams) {
                        command.Parameters.Add(sqlParam);
                    }

                    int res = (int)command.ExecuteScalar();
                    command.Parameters.Clear();

                    // create verification token
                    string token = Guid.NewGuid().ToString();
                    string otp_token = new Random().Next(1000, 9999).ToString();

                    command.CommandText = "INSERT INTO tbl_token(fk_user_id, otp, register_token, reset_token, expired_date, email) VALUES (@fk_user_id, @otp, @register_token, @reset_token, @expired_date, @email)";
                    sqlParams = new SqlParameter[]
                    {
                    new SqlParameter("@fk_user_id", SqlDbType.Int) { Value = (int) res },
                    new SqlParameter("@otp", SqlDbType.VarChar) { Value = otp_token },
                    new SqlParameter("@register_token", SqlDbType.VarChar) { Value = token },
                    new SqlParameter("@reset_token", SqlDbType.VarChar) { Value = token },
                    new SqlParameter("@email", SqlDbType.VarChar) { Value = user.email },
                    new SqlParameter("@expired_date", SqlDbType.DateTime) { Value = DateTime.Now.AddHours(1) },
                    };
                    foreach (SqlParameter sqlParam in sqlParams)
                    {
                        command.Parameters.Add(sqlParam);
                    }
                    command.ExecuteNonQuery();
                    trans.Commit();

                    // send verification token to email
                    string emailTo = user.email ?? "";
                    string emailSubject = "Account activation and OTP";
                    string verificationUrl = FrontUrl + "/email-confirm/" + token;
                    string otp_verif = otp_token;
                    string emailBody = @$"<div class='container'>
                    <img style='margin-top: 20; text-align: center; display: block; margin-left: auto; margin-right: auto;'
                        src='https://i.ibb.co/rdFr8CS/Asset-1xxxhdpi.png' alt'Asset-1xxxhdpi' width'20'
                    <h2>Click
                        the
                        button below to
                        activate your
                        account</h2>
                    <a href='{verificationUrl}' style='display: block; margin-left: auto; margin-right: auto;  text-decoration: none;'>
                        <div class='button'
                            style='padding: 10 20 10 20; border-radius: 7px; background-color: #790B0A; width:15%; margin-left: auto; margin-right: auto; font-size: 20; font-family: Poppins; font-weight: 400; color: white;'>
                            <p style='text-align: center; padding: 0; margin: 0;'>activate</p>
                        </div>
                    </a>
                    <h2 style='text-align: center; font-family: Poppins; font-weight: 400; margin-top: 50px; margin-left: 20px;'>OR ENTER OTP:</h2>
                    <h1 style='text-align: center; font-family: Poppins; font-weight: 400; margin-top: 50px; margin-left: 20px;'>{otp_verif}</h1>
                    <h3 style='text-align: center; font-family: Poppins; font-weight: 400; margin-top: 50px; margin-left: 20px;'>This
                        activation link will
                        be expired in two hours</h3>
                </div>";
                    EmailLogic.SendEmail(emailTo, emailSubject, emailBody);

                    return Ok("Success");
                }
                catch (Exception ex)
                {
                    trans.Rollback();
                    return BadRequest(ex.Message);
                }
            }
            
        }
        #endregion
       
        
    }
}
