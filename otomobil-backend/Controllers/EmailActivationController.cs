﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

using System.Data;
using System.Data.SqlClient;
//using System.Security.Cryptography; // ini untuk hash
using System.Text;

using otomobil_backend.Models;
using otomobil_backend.Logics;
using System.Security.Claims;

namespace otomobil_backend.Controllers
{
    [Route("api/")]
    [ApiController]
    public class EmailActivationController : ControllerBase
    {
        #region Token
        [HttpGet]
        [Route("email-confirm-token/{register_token}")]
        public ActionResult UserActivationByToken(string register_token)
        {
            try
            {
                // validasi: cek user belum ada di database
                string query = "SELECT TOP 1 email FROM tbl_token WHERE register_token=@token";
                SqlParameter[] sqlParams = new SqlParameter[]
                {
                    new SqlParameter("@token", SqlDbType.VarChar) { Value = register_token ?? "" },
                };

                object emailObj = CRUD.ExecuteScalar(query, sqlParams);
                string email = emailObj == DBNull.Value ? "" : (string)emailObj;

                if (String.IsNullOrEmpty(email))
                {
                    throw new Exception("Cannot activate the user, either the user is already active, or the user is not found");
                }

                // activate the user
                query = "UPDATE tbl_user SET confirmed_email=1 WHERE email=@email;";
                sqlParams = new SqlParameter[]
                {
                    new SqlParameter("@email", SqlDbType.VarChar) { Value = email },
                };
                CRUD.ExecuteNonQuery(query, sqlParams);

                return Ok("Success");
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
        #endregion
        #region OTP
        [HttpGet]
        [Route("email-confirm-otp/{otp_token}")]
        public ActionResult UserActivationByOTP(string otp_token)
        {
            try
            {
                // validasi: cek user belum ada di database
                string query = "SELECT TOP 1 email FROM tbl_token WHERE otp=@token";
                SqlParameter[] sqlParams = new SqlParameter[]
                {
                    new SqlParameter("@token", SqlDbType.VarChar) { Value = otp_token ?? "" },
                };

                object emailObj = CRUD.ExecuteScalar(query, sqlParams);
                string email = emailObj == DBNull.Value ? "" : (string)emailObj;

                if (String.IsNullOrEmpty(email))
                {
                    throw new Exception("Cannot activate the user, either the user is already active, or the user is not found");
                }

                // activate the user
                query = "UPDATE tbl_user SET confirmed_email = 1 WHERE email=@email;";
                sqlParams = new SqlParameter[]
                {
                    new SqlParameter("@email", SqlDbType.VarChar) { Value = email },
                };
                CRUD.ExecuteNonQuery(query, sqlParams);

                return Ok("Success");
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
        #endregion
    }
}
