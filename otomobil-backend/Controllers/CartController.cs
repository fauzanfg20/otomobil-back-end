﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using otomobil_backend.Logics;
using otomobil_backend.Models;
using System.Data;

namespace otomobil_backend.Controllers
{
    [Route("api/")]
    [ApiController]
    public class CartController : ControllerBase
    {
        [HttpGet]
        [Route("cart")]
        [Authorize(Roles = "admin, user")]
        public ActionResult GetCart([FromQuery] string? fk_user_id, string? car_id, string? schedule)
        {
            try
            {
                List<Cart> result = new List<Cart>(); // initialisasi array kosong
                result = CartLogic.GetCart(fk_user_id, car_id, schedule);
                return Ok(result);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        #region Insert Data
        [HttpPost]
        [Route("cart")]
        [Authorize(Roles = "admin, user")]
        public ActionResult AddCart([FromBody] CartInsert body)
        {
            try
            {
                CartLogic.AddCart(body);
                return Ok("Success");
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
        #endregion

        #region Delete Data
        [HttpDelete]
        [Route("cart/{id}")]
        [Authorize(Roles = "admin, user")]
        public ActionResult DeleteCarType([FromRoute] string id)
        {
            try
            {
                CartLogic.DeleteCart(id);
                return Ok("Success");
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
        #endregion
    }
}
