﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using otomobil_backend.Logics;
using otomobil_backend.Models;
using System.Data;

namespace otomobil_backend.Controllers
{
    [Route("api/")]
    [ApiController]
    public class UserController : Controller
    {
        #region Get Data
        [HttpGet]
        [Route("users")]
        [Authorize(Roles = "admin")]
        public ActionResult GetUser([FromQuery] string? user_id)
        {
            try
            {
                List<UserWithAuth> result = new List<UserWithAuth>(); // initialisasi array kosong
                result = UserLogic.GetUser(user_id);
                return Ok(result);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
        #endregion
        #region get token
        [HttpGet]
        [Route("get-token/{email}")]
        [Authorize(Roles = "admin")]
        public ActionResult GetToken(string? email)
        {
            try
            {
                List<UserResendEmail> result = new List<UserResendEmail>(); // initialisasi array kosong
                result = TokenLogic.GetToken(email);
                return Ok(result[0].otp);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
        #endregion

        #region Insert Data
        [HttpPost]
        [Route("users")]
        [Authorize(Roles = "admin")]
        public ActionResult AddUser([FromBody] UserAdd body)
        {
            try
            {
                string result = UserLogic.AddUser(body);
                if(result != "OK") return BadRequest(result);
                return Ok("Success");
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
        #endregion

        #region Update Data
        [HttpPut]
        [Route("users/{user_id}")]
        [Authorize(Roles = "admin")]
        public ActionResult EditUser([FromBody] UserAdd body, string user_id)
        {
            try
            {
                string result = UserLogic.EditUser(body, user_id);
                if (result != "OK") return BadRequest(result);
                return Ok("Success");
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
        #endregion

        #region Delete Data
        [HttpDelete]
        [Route("users/{user_id}")]
        [Authorize(Roles = "admin")]
        public ActionResult DeletePaymentMethod(string user_id)
        {
            try
            {
                UserLogic.DeleteUser(user_id);
                return Ok("Success");
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
        #endregion
    }
}
