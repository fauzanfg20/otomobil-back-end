﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using otomobil_backend.Logics;
using otomobil_backend.Models;
using System.Data;

namespace otomobil_backend.Controllers
{
    [Route("api/")]
    [ApiController]
    public class PaymentMethodController : ControllerBase
    {
        [HttpGet]
        [Route("payment-method")]
        [Authorize(Roles = "admin,user")]
        public ActionResult GetPaymentMethod([FromQuery] string? active)
        {
            try
            {
                List<PaymentMethod> result = new List<PaymentMethod>(); // initialisasi array kosong
                result = PaymentMethodLogic.GetPaymentMethods(active);
                return Ok(result);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        #region Insert Data
        [HttpPost]
        [Route("payment-method")]
        [Authorize(Roles = "admin")]
        public ActionResult AddPaymentMethod([FromBody] PaymentMethodInsert body)
        {
            try
            {
                PaymentMethodLogic.AddPaymentMethod(body);
                return Ok("Success");
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
        #endregion

        #region Update Data
        [HttpPut]
        [Route("payment-method/{id}")]
        [Authorize(Roles = "admin")]
        public ActionResult EditPaymentMethod([FromBody] PaymentMethodInsert body, [FromRoute] string id)
        {
            try
            {
                PaymentMethodLogic.EditPaymentMethod(body, id);
                return Ok("Success");
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
        #endregion

        #region Delete Data
        [HttpDelete]
        [Route("payment-method/{id}")]
        [Authorize(Roles = "admin")]
        public ActionResult DeletePaymentMethod([FromRoute] string id)
        {
            try
            {
                PaymentMethodLogic.DeletePaymentMethod(id);
                return Ok("Success");
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
        #endregion


        #region Update Data
        [HttpPatch]
        [Route("payment-method/{id}")]
        [Authorize(Roles = "admin")]
        public ActionResult PatchPaymentMethod([FromBody] PaymentMethodPatch body, [FromRoute] string id)
        {
            try
            {
                PaymentMethodLogic.PatchPaymentMethod(body, id);
                return Ok("Success");
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
        #endregion

    }
}
