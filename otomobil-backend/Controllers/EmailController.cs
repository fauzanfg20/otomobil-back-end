﻿using Microsoft.AspNetCore.Mvc;
using otomobil_backend.Logics;
using otomobil_backend.Models;
using System.Data.SqlClient;
using System.Data;

namespace otomobil_backend.Controllers
{
    [Route("api/")]
    [ApiController]
    public class EmailController : ControllerBase
    {
        private string FrontUrl = "";
        public EmailController(IConfiguration configuration)
        {
            FrontUrl = configuration["FrontUrl"];
        }
        #region resendAPIactivation
        [HttpGet]
        [Route("resend-activation/{email}")]
        public ActionResult ResendEmailConfirm(string? email)
        {
            try
            {
                // validasi: data input user tidak ada yang kosong
                if (String.IsNullOrEmpty(email))
                {
                    throw new Exception("Email cannot empty");
                }

                // validasi: cek user belum ada di database
                string query = "SELECT TOP 1 * FROM tbl_user WHERE email=@email";
                SqlParameter[] sqlParams = new SqlParameter[]
                {
                    new SqlParameter("@email", SqlDbType.VarChar) { Value = email ?? "" },
                };
                DataTable userTable = CRUD.ExecuteQuery(query, sqlParams);
                if (userTable.Rows.Count < 1)
                {
                    throw new Exception("User with email: " + email + " not registered");
                }

                List<UserResendEmail> result = new List<UserResendEmail>(); // initialisasi array kosong
                result = TokenLogic.GetToken(email);

                // send verification token to email
                string emailTo = email ?? "";
                string emailSubject = "Account activation and OTP";
                string verificationUrl = FrontUrl + "/email-confirm/" + result[0].register_token;
                string otp_verif = result[0].otp;
                string emailBody = @$"<div class='container'>
                    <img style='margin-top: 20; text-align: center; display: block; margin-left: auto; margin-right: auto;'
                        src='https://i.ibb.co/rdFr8CS/Asset-1xxxhdpi.png' alt'Asset-1xxxhdpi' width'20'
                    <h2>Click
                        the
                        button below to
                        activate your
                        account</h2>
                    <a href='{verificationUrl}' style='display: block; margin-left: auto; margin-right: auto;  text-decoration: none;'>
                        <div class='button'
                            style='padding: 10 20 10 20; border-radius: 7px; background-color: #790B0A; width:15%; margin-left: auto; margin-right: auto; font-size: 20; font-family: Poppins; font-weight: 400; color: white;'>
                            <p style='text-align: center; padding: 0; margin: 0;'>activate</p>
                        </div>
                    </a>
                    <h2 style='text-align: center; font-family: Poppins; font-weight: 400; margin-top: 50px; margin-left: 20px;'>OR ENTER OTP:</h2>
                    <h1 style='text-align: center; font-family: Poppins; font-weight: 400; margin-top: 50px; margin-left: 20px;'>{otp_verif}</h1>
                    <h3 style='text-align: center; font-family: Poppins; font-weight: 400; margin-top: 50px; margin-left: 20px;'>This
                        activation link will
                        be expired in two hours</h3>
                </div>";
                EmailLogic.SendEmail(emailTo, emailSubject, emailBody);

                return Ok("Success");
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
        #endregion

        #region resetPasswordEmailAPI
        [HttpGet]
        [Route("reset-passwordl/{email}")]
        public ActionResult ResetPassword(string? email)
        {
            try
            {
                // validasi: data input user tidak ada yang kosong
                if (String.IsNullOrEmpty(email))
                {
                    throw new Exception("Email cannot empty");
                }

                // validasi: cek user belum ada di database
                string query = "SELECT TOP 1 * FROM tbl_user WHERE email=@email";
                SqlParameter[] sqlParams = new SqlParameter[]
                {
                    new SqlParameter("@email", SqlDbType.VarChar) { Value = email ?? "" },
                };
                DataTable userTable = CRUD.ExecuteQuery(query, sqlParams);
                if (userTable.Rows.Count < 1)
                {
                    throw new Exception("User with email: " + email + " not registered");
                }

                List<UserResendEmail> result = new List<UserResendEmail>(); // initialisasi array kosong
                result = TokenLogic.GetToken(email);

                // send verification token to email
                string emailTo = email ?? "";
                string emailSubject = "Account activation and OTP";
                string verificationUrl = FrontUrl + "/new-password/" + result[0].register_token;
                string emailBody = @$"<div class='container'>
                    <img style='margin-top: 20; text-align: center; display: block; margin-left: auto; margin-right: auto;'
                        src='https://afexb.com/wp-content/uploads/2021/08/1167157.png' alt'Asset-1xxxhdpi' width'20'
                    <h2>Click

                    <a href='{verificationUrl}' style='display: block; margin-left: auto; margin-right: auto;  text-decoration: none;'>
                        <div class='button'
                            style='padding: 10 20 10 20; border-radius: 7px; background-color: #790B0A; width:15%; margin-left: auto; margin-right: auto; font-size: 20; font-family: Poppins; font-weight: 400; color: white;'>
                            <p style='text-align: center; padding: 0; margin: 0;'>Reset Password</p>
                        </div>
                    </a>
                    <h3 style='text-align: center; font-family: Poppins; font-weight: 400; margin-top: 50px; margin-left: 20px;'>This
                        reset link will
                        be expired in two hours</h3>
                </div>";
                EmailLogic.SendEmail(emailTo, emailSubject, emailBody);

                return Ok("Success");
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
        #endregion
    }
}
