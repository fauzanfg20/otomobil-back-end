﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using otomobil_backend.Logics;
using otomobil_backend.Models;
using System.Data;

namespace otomobil_backend.Controllers
{
    [Route("api/")]
    [ApiController]
    public class CourseController : ControllerBase
    {
        [HttpGet]
        [Route("course-list")]
        public ActionResult GetCourse([FromQuery] string? id, string? fk_car_type)
        {
            try
            {
                List<CourseWithNameType> result = new List<CourseWithNameType>(); // initialisasi array kosong
                result = CourseLogic.GetCourse(id, fk_car_type);
                return Ok(result);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        #region Insert Data
        [HttpPost]
        [Route("course-list")]
        [Authorize(Roles = "admin")]
        public ActionResult AddCourse([FromBody] CourseInsert body)
        {
            try
            {
                CourseLogic.AddCourse(body);
                return Ok("Success");
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
        #endregion

        #region Update Data
        [HttpPut]
        [Route("course-list/{id}")]
        [Authorize(Roles = "admin")]
        public ActionResult EditCarType([FromBody] CourseInsert body, [FromRoute] string id)
        {
            try
            {
                CourseLogic.EditCourse(body, id);
                return Ok("Success");
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
        #endregion

        #region Delete Data
        [HttpDelete]
        [Route("course-list/{id}")]
        [Authorize(Roles = "admin")]
        public ActionResult DeleteCarType([FromRoute] string id)
        {
            try
            {
                CourseLogic.DeleteCourse(id);
                return Ok("Success");
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
        #endregion
    }
}
