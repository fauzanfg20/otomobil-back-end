﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using otomobil_backend.Logics;
using otomobil_backend.Models;
using System.Data;

namespace otomobil_backend.Controllers
{
    [Route("api/")]
    [ApiController]
    public class InvoiceController : ControllerBase
    {
        [HttpGet]
        [Route("invoice")]
        [Authorize(Roles = "admin,user")]
        public ActionResult GetInvoices([FromQuery] string? user_id)
        {
            try
            {
                List<InvoiceWithDetail> result = new List<InvoiceWithDetail>();
                result = InvoiceLogic.GetInvoices(user_id);
                return Ok(result);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        #region Insert Data
        [HttpPost]
        [Route("invoice")]
        [Authorize(Roles = "admin,user")]
        public ActionResult AddInvoice([FromBody] InvoiceWithDetail body)
        {
            try
            {
                string status = InvoiceLogic.AddInvoices(body);
                if (status != "OK") { return BadRequest(status); }
                return Ok("Success");
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
        #endregion
    }
}
