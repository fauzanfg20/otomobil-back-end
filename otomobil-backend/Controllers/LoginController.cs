﻿using Microsoft.AspNetCore.Mvc;
using otomobil_backend.Logics;
using otomobil_backend.Models;
using System.Data.SqlClient;
using System.Data;
using System.Security.Claims;

namespace otomobil_backend.Controllers
{
    [Route("api/")]
    [ApiController]
    public class LoginController : ControllerBase
    {
        private string FrontUrl = "";
        public LoginController(IConfiguration configuration)
        {
            FrontUrl = configuration["FrontUrl"];
        }

        [HttpPost]
        [Route("Login")]
        public ActionResult Login([FromBody] UserRequest user)
        {
            try
            {
                // validasi
                if (String.IsNullOrEmpty(user.email))
                {
                    throw new Exception("Email cannot empty");
                }
                if (String.IsNullOrEmpty(user.password))
                {
                    throw new Exception("Password cannot empty");
                }

                // check user exist and active in database
                string query = "SELECT TOP 1 * FROM tbl_user WHERE [email]=@email";
                SqlParameter[] sqlParams = new SqlParameter[]
                {
                    new SqlParameter("@email", SqlDbType.VarChar) { Value = user.email ?? "" },
                };
                DataTable userTable = CRUD.ExecuteQuery(query, sqlParams);

                if (userTable.Rows.Count == 0)
                {
                    throw new Exception("No user with email: " + user.email);
                }
                //validasi confirmead_email dari protected route di FE
                /*if ((bool)userTable.Rows[0]["confirmed_email"] == false)
                {
                    throw new Exception("User " + user.email + " has not been activated yet");
                }*/

                // initialize user variable from database
                byte[] passwordHash = Array.Empty<byte>();
                byte[] passwordSalt = Array.Empty<byte>();
                foreach (DataRow userRow in userTable.Rows)
                {
                    passwordHash = userRow["password_hash"] == DBNull.Value ? Array.Empty<byte>() : (byte[])userRow["password_hash"];
                    passwordSalt = userRow["salt_hash"] == DBNull.Value ? Array.Empty<byte>() : (byte[])userRow["salt_hash"];
                    //user.role = userRow["role"] == DBNull.Value ? "" : (string)userRow["role"];
                }

                // compare password input from user with password in database
                bool isPasswordValid = CryptoLogic.CompareStringVsHash(user.password, passwordHash, passwordSalt);
                if (isPasswordValid == false)
                {
                    throw new Exception("Wrong password");
                }

                // create jwt token
                string jwtToken = JwtTokenLogic.GenerateJwtToken(new[]
                {
                    new Claim("user_id", userTable.Rows[0]["user_id"].ToString() ?? ""),
                    new Claim("email", user.email ?? ""),
                    new Claim("name", (string)userTable.Rows[0]["name"] ?? ""),
                    new Claim(ClaimTypes.Role, (string)userTable.Rows[0]["role"] ?? ""),
                    new Claim("confirmed_email", userTable.Rows[0]["confirmed_email"].ToString() ?? "")
                });
                return Ok(isPasswordValid ? jwtToken : "wrong password");
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

    }
}
