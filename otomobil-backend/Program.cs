using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.IdentityModel.Tokens;
using Microsoft.OpenApi.Models;
using System.Text;

var builder = WebApplication.CreateBuilder(args);

// BOOTCAMP - Get configuration from appsettings.json
IConfiguration configuration = builder.Configuration;

// BOOTCAMP - Initialize our class with configuration
otomobil_backend.Models.CRUD.GetConfiguration(configuration);
otomobil_backend.Logics.JwtTokenLogic.GetConfiguration(configuration);
otomobil_backend.Logics.EmailLogic.GetConfiguration(configuration);
otomobil_backend.Logics.CryptoLogic.GetConfiguration(configuration);
// Add services to the container.

builder.Services.AddControllers();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen(c =>
{
    c.SwaggerDoc("v1", new OpenApiInfo { Title = "Test01", Version = "v1" });

    c.AddSecurityDefinition("Bearer", new OpenApiSecurityScheme()
    {
        Name = "Authorization",
        Type = SecuritySchemeType.ApiKey,
        Scheme = "Bearer",
        BearerFormat = "JWT",
        In = ParameterLocation.Header,
        Description = "JWT Authorization header using the Bearer scheme."

    });
    c.AddSecurityRequirement(new OpenApiSecurityRequirement
                {
                    {
                          new OpenApiSecurityScheme
                          {
                              Reference = new OpenApiReference
                              {
                                  Type = ReferenceType.SecurityScheme,
                                  Id = "Bearer"
                              }
                          },
                         new string[] {}
                    }
                });
});

// BOOTCAMP - ADD AUTH
// source:
// https://www.infoworld.com/article/3669188/how-to-implement-jwt-authentication-in-aspnet-core-6.html
// https://www.youtube.com/watch?v=v7q3pEK1EA0 dan https://www.youtube.com/watch?v=TDY_DtTEkes
builder.Services.AddAuthentication(options =>
{
    options.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
    options.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
    options.DefaultScheme = JwtBearerDefaults.AuthenticationScheme;
}).AddJwtBearer(o =>
{
    o.TokenValidationParameters = new TokenValidationParameters
    {
        // validate credential
        ValidateIssuerSigningKey = true,
        IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(configuration["Jwt:SecretKey"])),
        // validate issuer
        ValidateIssuer = true,
        ValidIssuer = configuration["Jwt:Issuer"],
        // validate audience
        ValidateAudience = true,
        ValidAudience = configuration["Jwt:Audience"],
        // validate expire time
        ValidateLifetime = true,
        ClockSkew = TimeSpan.Zero,
    };
});

// BOOTCAMP - ADD CORS
// source: https://stackoverflow.com/questions/31942037/how-to-enable-cors-in-asp-net-core
builder.Services.AddCors(options =>
{
    options.AddPolicy("AllowAll",
        builder =>
        {
            builder
            .AllowAnyOrigin()
            .AllowAnyMethod()
            .AllowAnyHeader();
        });
});

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

// BOOTCAMP - ADD AUTH (HARUS SEBELUM app.UseAuthorization())
app.UseAuthentication();

app.UseAuthorization();

// BOOTCAMP - ADD CORS
app.UseCors("AllowAll");

app.MapControllers();

app.Run();
