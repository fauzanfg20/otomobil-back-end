﻿using otomobil_backend.Models;
using System.Data.SqlClient;
using System.Data;
using System.Globalization;

namespace otomobil_backend.Logics
{
    public class CartLogic
    {
        public static List<Cart> GetCart(string? fk_user_id, string? car_id, string? schedule)
        {
            List<Cart> result = new List<Cart>(); // initialisasi array kosong

            #region query process to database
            // handle query
            string query = "SELECT * FROM tbl_cart";
            if (!String.IsNullOrEmpty(fk_user_id) && String.IsNullOrEmpty(car_id) && String.IsNullOrEmpty(schedule))
            {
                query += " WHERE fk_user_id = @fk_user_id";
            }

            string formatedDateSchedule = "";

            if(!String.IsNullOrEmpty(fk_user_id) && !String.IsNullOrEmpty(car_id) && !String.IsNullOrEmpty(schedule)){
                query += " WHERE fk_course_id = @car_id AND schedule = @schedule AND fk_user_id=@fk_user_id";

                DateTime date = DateTime.ParseExact(schedule, "dddd, d MMMM yyyy", CultureInfo.InvariantCulture);
                formatedDateSchedule = date.ToString("yyyy-MM-dd");
            }
            

            // create sql params
            SqlParameter[] sqlParams = new SqlParameter[]
            {
                new SqlParameter("@fk_user_id", SqlDbType.VarChar) { Value = (fk_user_id ?? "") },
                new SqlParameter("@schedule", SqlDbType.VarChar) { Value = formatedDateSchedule ?? "" },
                new SqlParameter("@car_id", SqlDbType.VarChar) { Value = (car_id ?? "") }
            };

            // execute query and map the data to result
            DataTable dataTable = CRUD.ExecuteQuery(query, sqlParams);

            foreach (DataRow row in dataTable.Rows)
            {
                string queryCourse = "SELECT * FROM tbl_course WHERE course_id = @fk_course_id";

                SqlParameter[] sqlParamsCourse = new SqlParameter[]
                {
                    new SqlParameter("@fk_course_id", SqlDbType.Int) { Value =  (int)row["fk_course_id"]}
                };

                DataTable dataTableCourse = CRUD.ExecuteQuery(queryCourse, sqlParamsCourse);

                string queryType = "SELECT * FROM tbl_car_type WHERE car_type_id = @fk_car_type_id";

                int fk_car_type_id = (int)dataTableCourse.Rows[0]["fk_car_type_id"];

                SqlParameter[] sqlParamsType = new SqlParameter[]
                {
                    new SqlParameter("@fk_car_type_id", SqlDbType.Int) { Value =  fk_car_type_id}
                };

                DataTable dataTableType = CRUD.ExecuteQuery(queryType, sqlParamsType);



                Cart tempData = new Cart
                {
                    id = (int)row["cart_id"],
                    car_id = (int)dataTableCourse.Rows[0]["course_id"],
                    course_name = (string)dataTableCourse.Rows[0]["course_name"],
                    price = (int)dataTableCourse.Rows[0]["price"],
                    image = (string)dataTableCourse.Rows[0]["image"],
                    schedule = String.Format("{0:dddd, dd MMMM yyyy}", row["schedule"]),
                    type = (string)dataTableType.Rows[0]["car_type"],
                    user_id = (int)row["fk_user_id"]
                };

                result.Add(tempData);
            }
            #endregion

            return result;
        }

        public static void AddCart(CartInsert cart)
        {
            string query = "INSERT INTO tbl_cart(fk_user_id, fk_course_id, schedule) VALUES (@fk_user_id, @fk_course_id, @schedule)";

            DateTime date = DateTime.ParseExact(cart.schedule, "dddd, d MMMM yyyy", CultureInfo.InvariantCulture);
            string formattedDate = date.ToString("yyyy-MM-dd");

            SqlParameter[] sqlParams = new SqlParameter[]
            {
                new SqlParameter("@fk_user_id", SqlDbType.VarChar){ Value = cart.user_id },
                new SqlParameter("@fk_course_id", SqlDbType.VarChar){ Value = cart.car_id },
                new SqlParameter("@schedule", SqlDbType.VarChar){ Value = formattedDate },
            };

            CRUD.ExecuteNonQuery(query, sqlParams); // ExecuteNonQuery untuk query yang tidak return apa-apa
        }

        public static void DeleteCart(string id)
        {
            string query = "DELETE FROM tbl_cart WHERE cart_id=@id";
            SqlParameter[] sqlParams = new SqlParameter[]
            {
                new SqlParameter("@id", SqlDbType.VarChar){ Value = id }
            };

            CRUD.ExecuteNonQuery(query, sqlParams); // ExecuteNonQuery untuk query yang tidak return apa-apa
        }
    }
}
