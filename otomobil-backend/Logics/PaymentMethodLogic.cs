﻿using otomobil_backend.Models;
using System.Data.SqlClient;
using System.Data;

namespace otomobil_backend.Logics
{
    public class PaymentMethodLogic
    {
        public static List<PaymentMethod> GetPaymentMethods(string? active)
        {
            List<PaymentMethod> result = new List<PaymentMethod>(); // initialisasi array kosong

            #region query process to database
            // handle query
            string query = "SELECT * FROM payment_method";
            if (!String.IsNullOrEmpty(active))
            {
                query += " WHERE active = @active";
            }

            // create sql params
            SqlParameter[] sqlParams = new SqlParameter[]
            {
                new SqlParameter("@active", SqlDbType.VarChar) { Value = active ?? ""}
            };

            // execute query and map the data to result
            DataTable dataTable = CRUD.ExecuteQuery(query, sqlParams);

            foreach (DataRow row in dataTable.Rows)
            {
                PaymentMethod tempData = new PaymentMethod                {
                    id = (int)row["id"],
                    name = (string)row["name"],
                    icon = (string)row["icon"],
                    active = (bool)row["active"],
                };

                result.Add(tempData);
            }
            #endregion

            return result;
        }

        public static void AddPaymentMethod(PaymentMethodInsert paymentMethod)
        {
            string query = "INSERT INTO payment_method(name, icon, active) VALUES (@name, @icon, @active)";
            SqlParameter[] sqlParams = new SqlParameter[]
            {
                new SqlParameter("@name", SqlDbType.VarChar){ Value = paymentMethod.name },
                new SqlParameter("@icon", SqlDbType.VarChar){ Value = paymentMethod.icon },
                new SqlParameter("@active", SqlDbType.Bit){ Value = paymentMethod.active }
            };

            CRUD.ExecuteNonQuery(query, sqlParams); // ExecuteNonQuery untuk query yang tidak return apa-apa
        }

        public static void EditPaymentMethod(PaymentMethodInsert paymentMethod, string id)
        {
            string query = "UPDATE payment_method SET name=@name, icon=@icon, active=@active WHERE id=@id";
            SqlParameter[] sqlParams = new SqlParameter[]
            {
                new SqlParameter("@name", SqlDbType.VarChar){ Value = paymentMethod.name },
                new SqlParameter("@icon", SqlDbType.VarChar){ Value = paymentMethod.icon },
                new SqlParameter("@active", SqlDbType.Bit){ Value = paymentMethod.active },
                new SqlParameter("@id", SqlDbType.VarChar){ Value = id }
            };

            CRUD.ExecuteNonQuery(query, sqlParams); // ExecuteNonQuery untuk query yang tidak return apa-apa
        }

        public static void DeletePaymentMethod(string id)
        {
            string query = "DELETE FROM payment_method WHERE id=@id";
            SqlParameter[] sqlParams = new SqlParameter[]
            {
                new SqlParameter("@id", SqlDbType.VarChar){ Value = id }
            };

            CRUD.ExecuteNonQuery(query, sqlParams); // ExecuteNonQuery untuk query yang tidak return apa-apa
        }

        public static void PatchPaymentMethod(PaymentMethodPatch paymentMethod, string id)
        {
            string query = "UPDATE payment_method SET active=@active WHERE id=@id";
            SqlParameter[] sqlParams = new SqlParameter[]
            {
                new SqlParameter("@active", SqlDbType.Bit){ Value = paymentMethod.active },
                new SqlParameter("@id", SqlDbType.VarChar){ Value = id }
            };

            CRUD.ExecuteNonQuery(query, sqlParams); // ExecuteNonQuery untuk query yang tidak return apa-apa
        }
    }
}
