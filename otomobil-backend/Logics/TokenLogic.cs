﻿using otomobil_backend.Models;
using System.Data.SqlClient;
using System.Data;


namespace otomobil_backend.Logics
{
    public class TokenLogic
    {
    public static List<UserResendEmail> GetToken(string? email)
    {
        List<UserResendEmail> result = new List<UserResendEmail>(); // initialisasi array kosong

        #region query process to database
        // handle query
        string query = "SELECT * FROM tbl_token WHERE email=@email";

        // create sql params
        SqlParameter[] sqlParams = new SqlParameter[]
        {
                new SqlParameter("@email", SqlDbType.VarChar) { Value = email}
        };

        // execute query and map the data to result
        DataTable dataTable = CRUD.ExecuteQuery(query, sqlParams);

        foreach (DataRow row in dataTable.Rows)
        {
            UserResendEmail tempData = new UserResendEmail
            {
                register_token = (string)row["register_token"],
                otp = (string)row["otp"],
                reset_token = (string)row["reset_token"]
            };

            result.Add(tempData);
        }
        #endregion

        return result;
    }
}
}
