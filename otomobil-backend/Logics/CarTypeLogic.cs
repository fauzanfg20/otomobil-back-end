﻿using otomobil_backend.Models;
using System.Data.SqlClient;
using System.Data;

namespace otomobil_backend.Logics
{
    public class CarTypeLogic
    {
        public static List<CarType> GetCarType(string? id)
        {
            List<CarType> result = new List<CarType>(); // initialisasi array kosong

            #region query process to database
            // handle query
            string query = "SELECT * FROM tbl_car_type";
            if (!String.IsNullOrEmpty(id))
            {
                query += " WHERE car_type_id = @id";
            }

            // create sql params
            SqlParameter[] sqlParams = new SqlParameter[]
            {
                new SqlParameter("@id", SqlDbType.VarChar) { Value = (id ?? "") }
            };

            // execute query and map the data to result
            DataTable dataTable = CRUD.ExecuteQuery(query, sqlParams);

            foreach (DataRow row in dataTable.Rows)
            {
                CarType tempData = new CarType
                {
                    id = (int)row["car_type_id"],
                    name = (string)row["car_type"],
                    thumbnail = (string)row["thumbnail"],
                    img = (string)row["banner_image"],
                    desc = (string)row["car_type_description"],
                };

                result.Add(tempData);
            }
            #endregion

            return result;
        }

        public static void AddCarType(CarTypeInsert carType)
        {
            string query = "INSERT INTO tbl_car_type(car_type, thumbnail, banner_image, [car_type_description]) VALUES (@car_type, @thumbnail, @banner_image, @car_type_description)";
            SqlParameter[] sqlParams = new SqlParameter[]
            {
                new SqlParameter("@car_type", SqlDbType.VarChar){ Value = carType.name },
                new SqlParameter("@thumbnail", SqlDbType.VarChar){ Value = carType.thumbnail },
                new SqlParameter("@banner_image", SqlDbType.VarChar){ Value = carType.img },
                new SqlParameter("@car_type_description", SqlDbType.VarChar){ Value = carType.desc }
            };

            CRUD.ExecuteNonQuery(query, sqlParams); // ExecuteNonQuery untuk query yang tidak return apa-apa
        }

        public static void EditCarType(CarTypeInsert carType, string id)
        {
            string query = "UPDATE tbl_car_type SET car_type=@car_type, thumbnail=@thumbnail, banner_image=@banner_image, car_type_description=@car_type_description WHERE car_type_id=@id";
            SqlParameter[] sqlParams = new SqlParameter[]
            {
                new SqlParameter("@car_type", SqlDbType.VarChar){ Value = carType.name },
                new SqlParameter("@thumbnail", SqlDbType.VarChar){ Value = carType.thumbnail },
                new SqlParameter("@banner_image", SqlDbType.VarChar){ Value = carType.img },
                new SqlParameter("@car_type_description", SqlDbType.VarChar){ Value = carType.desc },
                new SqlParameter("@id", SqlDbType.VarChar){ Value = id }
            };

            CRUD.ExecuteNonQuery(query, sqlParams); // ExecuteNonQuery untuk query yang tidak return apa-apa
        }

        public static void DeleteCarType(string id)
        {
            string query = "DELETE FROM tbl_car_type WHERE car_type_id=@id";
            SqlParameter[] sqlParams = new SqlParameter[]
            {
                new SqlParameter("@id", SqlDbType.VarChar){ Value = id }
            };

            CRUD.ExecuteNonQuery(query, sqlParams); // ExecuteNonQuery untuk query yang tidak return apa-apa
        }
    }
}
