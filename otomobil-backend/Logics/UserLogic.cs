﻿using otomobil_backend.Models;
using System.Data.SqlClient;
using System.Data;
using System;
using Microsoft.AspNetCore.Identity;
using System.Diagnostics;

namespace otomobil_backend.Logics
{
    public class UserLogic
    {
        #region get user
        public static List<UserWithAuth> GetUser(string? user_id)
        {
            List<UserWithAuth> result = new List<UserWithAuth>(); // initialisasi array kosong

            #region query process to database
            // handle query
            string query = "SELECT * FROM tbl_user";
            if (!String.IsNullOrEmpty(user_id))
            {
                query += " WHERE user_id = @user_id";
            }

            // create sql params
            SqlParameter[] sqlParams = new SqlParameter[]
            {
                new SqlParameter("@user_id", SqlDbType.VarChar) { Value = (user_id ?? "")}
            };

            // execute query and map the data to result
            DataTable dataTable = CRUD.ExecuteQuery(query, sqlParams);

            foreach (DataRow row in dataTable.Rows)
            {
                UserWithAuth tempData = new UserWithAuth
                {
                    user_id = (int)row["user_id"],
                    name = (string)row["name"],
                    email = (string)row["email"],
                    can_be_admin = (bool)row["can_be_admin"],
                    role = (string)row["role"],
                    confirmed_email = (bool)row["confirmed_email"]
                };

                result.Add(tempData);
            }

            return result;
            #endregion
        }
        #endregion

        #region add user
        public static String AddUser(UserAdd user)
        {
                // validasi: data input user tidak ada yang kosong
                if (String.IsNullOrEmpty(user.name))
                {
                    throw new Exception("Name cannot empty");
                }
                if (String.IsNullOrEmpty(user.email))
                {
                    throw new Exception("Email cannot empty");
                }
                if (String.IsNullOrEmpty(user.password))
                {
                    throw new Exception("Password cannot empty");
                }
                if (String.IsNullOrEmpty(user.role))
                {
                    throw new Exception("Role cannot empty");
                }

                // validasi: cek user belum ada di database
                string query = "SELECT TOP 1 * FROM tbl_user WHERE email=@email";
                SqlParameter[] sqlParams = new SqlParameter[]
                {
                    new SqlParameter("@email", SqlDbType.VarChar) { Value = user.email ?? "" },
                };
                DataTable userTable = CRUD.ExecuteQuery(query, sqlParams);
                if (userTable.Rows.Count > 0)
                {
                    throw new Exception("User with email: " + user.email + " already exist");
                }
            
                using (SqlConnection con = CRUD.GenerateConnection())
                {
                    con.Open();
                    SqlTransaction trans = con.BeginTransaction();
                    SqlCommand command = con.CreateCommand();
                    command.Transaction = trans;
                    try
                    {
                    // create hash
                    byte[] passwordHash = Array.Empty<byte>();
                    byte[] passwordSalt = Array.Empty<byte>();

                    //(passwordHash, passwordSalt) = CryptoLogic.GenerateHash(user.password); // cara 1
                    CryptoLogic.GenerateHash(user.password ?? "", out passwordHash, out passwordSalt); // cara 2

                    // insert key and hash to db
                    command.CommandText = "INSERT INTO tbl_user([name], email, confirmed_email, password_hash, salt_hash, can_be_admin, role) OUTPUT INSERTED.user_id VALUES (@name, @email, @confirmed_email, @password_hash, @salt_hash, 1, @role)";

                    command.Parameters.Add(new SqlParameter("@email", SqlDbType.VarChar) { Value = user.email ?? "" });
                    command.Parameters.Add(new SqlParameter("@name", SqlDbType.VarChar) { Value = user.name ?? "" });
                    command.Parameters.Add(new SqlParameter("@confirmed_email", SqlDbType.Bit) { Value = user.confirmed_email });
                    command.Parameters.Add(new SqlParameter("@password_hash", SqlDbType.VarBinary) { Value = passwordHash });
                    command.Parameters.Add(new SqlParameter("@salt_hash", SqlDbType.VarBinary) { Value = passwordSalt });
                    command.Parameters.Add(new SqlParameter("@role", SqlDbType.VarChar) { Value = user.role ?? "" });

                    int res = (int)command.ExecuteScalar();
                    command.Parameters.Clear();

                    // create verification token
                    string token = Guid.NewGuid().ToString();
                    string otp_token = new Random().Next(1000, 9999).ToString();

                    command.CommandText = "INSERT INTO tbl_token(fk_user_id, otp, register_token, reset_token, expired_date, email) VALUES (@fk_user_id, @otp, @register_token, @reset_token, @expired_date, @email)";

                    command.Parameters.Add(new SqlParameter("@fk_user_id", SqlDbType.Int) { Value = (int)res });
                    command.Parameters.Add(new SqlParameter("@otp", SqlDbType.VarChar) { Value = otp_token });
                    command.Parameters.Add(new SqlParameter("@register_token", SqlDbType.VarChar) { Value = token });
                    command.Parameters.Add(new SqlParameter("@reset_token", SqlDbType.VarChar) { Value = token });
                    command.Parameters.Add(new SqlParameter("@email", SqlDbType.VarChar) { Value = user.email });
                    command.Parameters.Add(new SqlParameter("@expired_date", SqlDbType.DateTime) { Value = DateTime.Now.AddHours(1) });

                    command.ExecuteNonQuery();// ExecuteNonQuery untuk query yang tidak return apa-apa
                    trans.Commit();
                    }

                    catch (Exception ex)
                    {
                        trans.Rollback();
                        return ex.Message;
                    }
                
                }
            return "OK";
            #endregion

        }
            #region update user
            public static string EditUser(UserAdd user, string user_id)
            {
                using (SqlConnection con = CRUD.GenerateConnection())
            {
                con.Open();
                SqlTransaction trans = con.BeginTransaction();
                SqlCommand command = con.CreateCommand();
                command.Transaction = trans;
                try
                {
                    // create hash
                    byte[] passwordHash = Array.Empty<byte>();
                    byte[] passwordSalt = Array.Empty<byte>();

                    //(passwordHash, passwordSalt) = CryptoLogic.GenerateHash(user.password); // cara 1
                    CryptoLogic.GenerateHash(user.password ?? "", out passwordHash, out passwordSalt); // cara 2

                    command.CommandText = "UPDATE tbl_user SET [name]=@name, email=@email, confirmed_email=@confirmed_email, password_hash=@password_hash, salt_hash=@salt_hash, role=@role WHERE user_id=@user_id";

                    command.Parameters.Add(new SqlParameter("@email", SqlDbType.VarChar) { Value = user.email ?? "" });
                    command.Parameters.Add(new SqlParameter("@name", SqlDbType.VarChar) { Value = user.name ?? "" });
                    command.Parameters.Add(new SqlParameter("@confirmed_email", SqlDbType.Bit) { Value = user.confirmed_email });
                    command.Parameters.Add(new SqlParameter("@password_hash", SqlDbType.VarBinary) { Value = passwordHash });
                    command.Parameters.Add(new SqlParameter("@salt_hash", SqlDbType.VarBinary) { Value = passwordSalt });
                    command.Parameters.Add(new SqlParameter("@role", SqlDbType.VarChar) { Value = user.role ?? "" });
                    command.Parameters.Add(new SqlParameter("@user_id", SqlDbType.VarChar) { Value = user_id ?? "" });
                   

                    command.ExecuteNonQuery(); // ExecuteNonQuery untuk query yang tidak return apa-apa
                    command.Parameters.Clear();

                    command.CommandText = "UPDATE tbl_token SET email=@email WHERE fk_user_id=@user_id";

                    command.Parameters.Add(new SqlParameter("@email", SqlDbType.VarChar) { Value = user.email ?? "" });
                    command.Parameters.Add(new SqlParameter("@user_id", SqlDbType.VarChar) { Value = user_id ?? "" });


                    command.ExecuteNonQuery(); // ExecuteNonQuery untuk query yang tidak return apa-apa
                    trans.Commit();
                }
                catch(Exception ex)
                {
                    trans.Rollback();
                    return ex.Message;
                }
            }
            return "OK";
            }

        #endregion

        #region delete user
        public static void DeleteUser(string user_id)
        {
            string query = "DELETE FROM tbl_user WHERE user_id=@user_id";
            SqlParameter[] sqlParams = new SqlParameter[]
            {
                new SqlParameter("@user_id", SqlDbType.VarChar){ Value = user_id }
            };
            CRUD.ExecuteNonQuery(query, sqlParams);
        }
        #endregion
    }
}
