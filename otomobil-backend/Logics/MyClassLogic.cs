﻿using otomobil_backend.Models;
using System.Data.SqlClient;
using System.Data;
using System.Globalization;

namespace otomobil_backend.Logics
{
    public class MyClassLogic
    {
        public static List<DetailInvoice> GetUserClasses(string? id, string? course_id, string? schedule)
        {
            string query = "SELECT i.invoice_id, i.fk_user_id, d.course_id, " +
                            "d.course_name, d.car_type, d.schedule, d.image, d.price " +
                            "FROM tbl_invoice i JOIN tbl_invoice_detail d " +
                            "ON i.invoice_id = d.fk_invoice_id";

            List<string> queryParams = new();

            if (!String.IsNullOrEmpty(id))
            {
                queryParams.Add("i.fk_user_id = @id");
            };
            
            if(!String.IsNullOrEmpty(course_id))
            {
                queryParams.Add("d.course_id = @course_id");
            }

            string formatedDateSchedule = "";
            if (!String.IsNullOrEmpty(schedule))
            {
                queryParams.Add("d.schedule = @schedule");
                DateTime date = DateTime.ParseExact(schedule, "dddd, d MMMM yyyy", CultureInfo.InvariantCulture);
                formatedDateSchedule = date.ToString("yyyy-MM-dd");
            }

            if (queryParams.Count > 0)
            {
                query += " WHERE " + String.Join(" AND ", queryParams.ToArray());
            }

            SqlParameter[] sqlParams = new SqlParameter[]
            {
                new SqlParameter("@id", SqlDbType.VarChar) { Value = (id ?? "")},
                new SqlParameter("@course_id", SqlDbType.VarChar) { Value = (course_id ?? "")},
                new SqlParameter("@schedule", SqlDbType.VarChar) { Value = (formatedDateSchedule ?? "")}
            };

            DataTable dataTable = CRUD.ExecuteQuery(query, sqlParams);
            List<DetailInvoice> course_list = new();

            foreach (DataRow row in dataTable.Rows) 
            {
                int invoice_id = (int)row["invoice_id"];
                var course = new DetailInvoice
                {
                    fk_invoice_id = invoice_id,
                    fk_user_id = (int)row["fk_user_id"],
                    course_id = (int)row["course_id"],
                    course_name = (string)row["course_name"],
                    car_type = (string)row["car_type"],
                    schedule = (DateTime)row["schedule"],
                    price = (int)row["price"],
                    image = (string)row["image"]
                };
                course_list.Add(course);
            }
            return course_list;
        }
    }
}
