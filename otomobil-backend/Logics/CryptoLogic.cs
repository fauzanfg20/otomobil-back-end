﻿using System.Security.Cryptography;
using System.Text;
using System.Linq;

namespace otomobil_backend.Logics
{
    // source: https://www.youtube.com/watch?v=v7q3pEK1EA0
    public static class CryptoLogic
    {
        #region Get configuration from appsettings.json (butuh di register di Program.cs)
        private static string AesKey = "";
        private static string AesIv = "";

        public static void GetConfiguration(IConfiguration configuration)
        {
            AesKey = configuration["Aes:Key"];
            AesIv = configuration["Aes:Iv"];
        }
        #endregion

        #region hash
        public static (byte[] hash, byte[] salt) GenerateHash(string passwordString)
        {
            // hash menggunakan algoritma HMAC
            using (var hmac = new HMACSHA512())
            {
                // prepare key and hash
                byte[] passwordSalt = hmac.Key;
                byte[] passwordHash = hmac.ComputeHash(Encoding.UTF8.GetBytes(passwordString));
                return (passwordHash, passwordSalt);
            }
        }

        public static void GenerateHash(string passwordString, out byte[] passwordHash, out byte[] passwordSalt)
        {
            using (var hmac = new HMACSHA512())
            {
                passwordSalt = hmac.Key;
                passwordHash = hmac.ComputeHash(Encoding.UTF8.GetBytes(passwordString));
            }
        }

        public static bool CompareStringVsHash(string passwordString, byte[] passwordHash, byte[] passwordSalt)
        {
            using (var hmac = new HMACSHA512(passwordSalt))
            {
                byte[] newPassword = hmac.ComputeHash(Encoding.UTF8.GetBytes(passwordString));
                return newPassword.SequenceEqual(passwordHash);
            }
        }
        #endregion

        #region aes encryption
        // source: https://learn.microsoft.com/en-us/dotnet/api/system.security.cryptography.aes?view=net-6.0
        public static string DecryptStringFromBytes_Aes(string chiperTextStr)
        {
            // Check arguments.
            byte[] cipherText = Convert.FromBase64String(chiperTextStr);

            // Declare the string used to hold
            // the decrypted text.
            string plaintext = null;

            // Create an Aes object
            // with the specified key and IV.
            using (Aes aesAlg = Aes.Create())
            {
                aesAlg.Key = Encoding.UTF8.GetBytes(AesKey);
                aesAlg.IV = Encoding.UTF8.GetBytes(AesIv);
                aesAlg.Mode = CipherMode.CBC;
                aesAlg.Padding = PaddingMode.PKCS7;
                aesAlg.FeedbackSize = 128;

                // Create a decryptor to perform the stream transform.
                ICryptoTransform decryptor = aesAlg.CreateDecryptor(aesAlg.Key, aesAlg.IV);

                // Create the streams used for decryption.
                using (MemoryStream msDecrypt = new MemoryStream(cipherText))
                {
                    using (CryptoStream csDecrypt = new CryptoStream(msDecrypt, decryptor, CryptoStreamMode.Read))
                    {
                        using (StreamReader srDecrypt = new StreamReader(csDecrypt))
                        {

                            // Read the decrypted bytes from the decrypting stream
                            // and place them in a string.
                            plaintext = srDecrypt.ReadToEnd();
                        }
                    }
                }
            }

            return plaintext;
        }
        #endregion
    }
}
