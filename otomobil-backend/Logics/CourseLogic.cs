﻿using otomobil_backend.Models;
using System.Data.SqlClient;
using System.Data;
using static System.Net.Mime.MediaTypeNames;

namespace otomobil_backend.Logics
{
    public class CourseLogic
    {
        public static List<CourseWithNameType> GetCourse(string? id, string? fk_car_type)
        {
            List<CourseWithNameType> result = new List<CourseWithNameType>(); // initialisasi array kosong

            #region query process to database
            // handle query
            string query = "SELECT * FROM tbl_course";
            if (!String.IsNullOrEmpty(id))
            {
                query += " WHERE course_id = @id";
            }
            if(String.IsNullOrEmpty(id) && !String.IsNullOrEmpty(fk_car_type))
            {
                query += " WHERE fk_car_type_id = @fk_car_type";
            }

            // create sql params
            SqlParameter[] sqlParams = new SqlParameter[]
            {
                new SqlParameter("@id", SqlDbType.VarChar) { Value = (id ?? "") },
                new SqlParameter("@fk_car_type", SqlDbType.VarChar) { Value = (fk_car_type ?? "") }
            };

            // execute query and map the data to result
            DataTable dataTable = CRUD.ExecuteQuery(query, sqlParams);

            foreach (DataRow row in dataTable.Rows)
            {
                string queryType = "SELECT * FROM tbl_car_type WHERE car_type_id = @car_type_id";

                SqlParameter[] sqlParamsType = new SqlParameter[]
                {
                    new SqlParameter("@car_type_id", SqlDbType.Int) { Value =  (int)row["fk_car_type_id"]}
                };

                DataTable dataTableType = CRUD.ExecuteQuery(queryType, sqlParamsType);

                CourseWithNameType tempData = new CourseWithNameType
                {
                    id = (int)row["course_id"],
                    course_name = (string)row["course_name"],
                    fk_car_type_id = (int)row["fk_car_type_id"],
                    desc = (string)row["course_description"],
                    image = (string)row["image"],
                    price = (int)row["price"],
                    type = (string)dataTableType.Rows[0]["car_type"]
                };

                result.Add(tempData);
            }
            #endregion

            return result;
        }

        public static void AddCourse(CourseInsert course)
        {
            string query = "INSERT INTO tbl_course(course_name, fk_car_type_id, course_description, [image], price) VALUES (@course_name, @fk_car_type_id, @course_description, @image, @price)";
            SqlParameter[] sqlParams = new SqlParameter[]
            {
                new SqlParameter("@course_name", SqlDbType.VarChar){ Value = course.course_name },
                new SqlParameter("@fk_car_type_id", SqlDbType.VarChar){ Value = course.fk_car_type_id },
                new SqlParameter("@course_description", SqlDbType.VarChar){ Value = course.desc },
                new SqlParameter("@image", SqlDbType.VarChar){ Value = course.image },
                new SqlParameter("@price", SqlDbType.Int){ Value = course.price }
            };

            CRUD.ExecuteNonQuery(query, sqlParams); // ExecuteNonQuery untuk query yang tidak return apa-apa
        }

        public static void EditCourse(CourseInsert course, string id)
        {
            string query = "UPDATE tbl_course SET course_name=@course_name, fk_car_type_id=@fk_car_type_id, course_description=@course_description, [image]=@image, price=@price WHERE course_id=@id";
            SqlParameter[] sqlParams = new SqlParameter[]
            {
                new SqlParameter("@course_name", SqlDbType.VarChar){ Value = course.course_name },
                new SqlParameter("@fk_car_type_id", SqlDbType.VarChar){ Value = course.fk_car_type_id },
                new SqlParameter("@course_description", SqlDbType.VarChar){ Value = course.desc },
                new SqlParameter("@image", SqlDbType.VarChar){ Value = course.image },
                new SqlParameter("@price", SqlDbType.Int){ Value = course.price },
                new SqlParameter("@id", SqlDbType.VarChar){ Value = id },
            };

            CRUD.ExecuteNonQuery(query, sqlParams); // ExecuteNonQuery untuk query yang tidak return apa-apa
        }

        public static void DeleteCourse(string id)
        {
            string query = "DELETE FROM tbl_course WHERE course_id=@id";
            SqlParameter[] sqlParams = new SqlParameter[]
            {
                new SqlParameter("@id", SqlDbType.VarChar){ Value = id }
            };

            CRUD.ExecuteNonQuery(query, sqlParams); // ExecuteNonQuery untuk query yang tidak return apa-apa
        }
    }
}
