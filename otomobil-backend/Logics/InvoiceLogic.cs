﻿using otomobil_backend.Models;
using System.Data.SqlClient;
using System.Data;
using System;
using System.Collections.Generic;

namespace otomobil_backend.Logics
{
    public class InvoiceLogic
    {
        #region get user's invoices
        public static List<InvoiceWithDetail> GetInvoices(string? id)
        {
            string query = "SELECT i.invoice_id, i.fk_user_id, i.user_name, i.user_email, i.invoice_date, i.total_price, i.payment_method, " + 
                             "d.fk_invoice_id, d.cart_id, d.course_id, d.course_name, d.car_type, d.schedule, d.price, d.image " +
                             "FROM tbl_invoice i JOIN tbl_invoice_detail d " +
                             "ON i.invoice_id = d.fk_invoice_id";
            if (!String.IsNullOrEmpty(id))
            {
                query += " WHERE i.fk_user_id = @id";
            };

            SqlParameter[] sqlParams = new SqlParameter[]
            {
                new SqlParameter("@id", SqlDbType.VarChar) { Value = (id ?? "")}
            };

            DataTable dataTable = CRUD.ExecuteQuery(query, sqlParams);
            Dictionary<int, InvoiceWithDetail> invoices = new Dictionary<int, InvoiceWithDetail>();

            foreach (DataRow row in dataTable.Rows) 
            {
                int invoice_id = (int)row["invoice_id"];
                if (!invoices.ContainsKey(invoice_id)) 
                {
                    var invoice = new InvoiceWithDetail
                    {
                        invoice_id = (int)row["invoice_id"],
                        user_name = (string)row["user_name"],
                        user_email = (string)row["user_email"],
                        invoice_date = (DateTime)row["invoice_date"],
                        total_price = (int)row["total_price"],
                        payment_method = (string)row["payment_method"],
                        course_list = new List<DetailInvoice>(),
                    };
                    invoices[invoice_id] = invoice;
                }

                var course = new DetailInvoice()
                {
                    fk_invoice_id = invoice_id,
                    fk_user_id = (int)row["fk_user_id"],
                    cart_id = (int)row["cart_id"],
                    course_id = (int)row["course_id"],
                    course_name = (string)row["course_name"],
                    car_type = (string)row["car_type"],
                    schedule = (DateTime) row["schedule"],
                    price = (int)row["price"],
                    image = (string)row["image"]
                };

                invoices[invoice_id].course_list.Add(course);
            }

            List<InvoiceWithDetail> result = new List<InvoiceWithDetail>();
            foreach (InvoiceWithDetail invoiceWithDetail in invoices.Values) {
                result.Add(invoiceWithDetail);
            }
            return result;
        }
        #endregion

        #region Add Invoice with Details
        public static string AddInvoices(InvoiceWithDetail invoiceWithDetail) {
            using (SqlConnection con = CRUD.GenerateConnection())
            {
                con.Open();
                SqlTransaction trans = con.BeginTransaction();
                SqlCommand command = con.CreateCommand();
                command.Transaction = trans;
                try
                {
                    command.CommandText = "INSERT INTO tbl_invoice(fk_user_id, user_name, user_email, invoice_date, total_price, payment_method)"
                                           + " VALUES(@fk_user_id, @user_name, @user_email, @invoice_date, @total_price, @payment_method); SELECT CAST(SCOPE_IDENTITY() AS INT);";
                    command.Parameters.Add(new SqlParameter("@fk_user_id", SqlDbType.Int) { Value = invoiceWithDetail.fk_user_id });
                    command.Parameters.Add(new SqlParameter("@user_name", SqlDbType.VarChar) { Value = invoiceWithDetail.user_name});
                    command.Parameters.Add(new SqlParameter("@user_email", SqlDbType.VarChar) { Value = invoiceWithDetail.user_email});
                    command.Parameters.Add(new SqlParameter("@invoice_date", SqlDbType.DateTime) { Value = invoiceWithDetail.invoice_date });
                    command.Parameters.Add(new SqlParameter("@total_price", SqlDbType.Int) { Value = invoiceWithDetail.total_price });
                    command.Parameters.Add(new SqlParameter("@payment_method", SqlDbType.VarChar) { Value = invoiceWithDetail.payment_method});
                    int invoice_id = (int)command.ExecuteScalar();

                    string prepend = "INSERT INTO tbl_invoice_detail(fk_invoice_id, cart_id, course_id, course_name, car_type, price, schedule, image) VALUES ";
                    command.CommandText = prepend;

                    var course_list = invoiceWithDetail.course_list;

                    for (int index = 0; index < course_list.Count; index++)
                    {
                        DetailInvoice detailInvoice = course_list[index];
                        var idx = index.ToString();
                        string values_format = "(@fk_invoice_id_{0}, @cart_id_{0}, @course_id_{0}, @course_name_{0}, @car_type_{0}, @price_{0}, @schedule_{0}, @image_{0}),";

                        command.CommandText += string.Format(values_format, index);
                        command.Parameters.Add(new SqlParameter("@fk_invoice_id_" + index, SqlDbType.Int) { Value = invoice_id });
                        command.Parameters.Add(new SqlParameter("@fk_user_id_" + index, SqlDbType.Int) { Value = invoiceWithDetail.fk_user_id});
                        command.Parameters.Add(new SqlParameter("@cart_id_" + index, SqlDbType.Int) { Value = detailInvoice.cart_id });
                        command.Parameters.Add(new SqlParameter("@course_id_" + index, SqlDbType.Int) { Value = detailInvoice.course_id });
                        command.Parameters.Add(new SqlParameter("@course_name_" + index, SqlDbType.VarChar) { Value = detailInvoice.course_name });
                        command.Parameters.Add(new SqlParameter("@car_type_" + index, SqlDbType.VarChar) { Value = detailInvoice.car_type });
                        command.Parameters.Add(new SqlParameter("@price_" + index, SqlDbType.Int) { Value = detailInvoice.price });
                        command.Parameters.Add(new SqlParameter("@schedule_" + index, SqlDbType.DateTime) { Value = detailInvoice.schedule });
                        command.Parameters.Add(new SqlParameter("@image_" + index, SqlDbType.VarChar) { Value = detailInvoice.image});
                    }
                    command.CommandText = command.CommandText.TrimEnd(',') + ";";
                    command.ExecuteNonQuery();
                    trans.Commit();
                }
                catch (Exception ex)
                {
                    System.Diagnostics.Debug.WriteLine("Command Text:", command.CommandText);
                    System.Diagnostics.Debug.WriteLine("EXCEPTION:", ex.Message);
                    trans.Rollback();
                    return ex.Message;
                }
            }
            return "OK";
        }
        #endregion
    }
}