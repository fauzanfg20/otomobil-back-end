﻿using System;

namespace otomobil_backend.Models
{
	public class DetailInvoice
	{
		public int fk_invoice_id { get; set; }
		public int fk_user_id { get; set; }
		public int cart_id { get; set; }
		public int course_id { get; set; }
		public int car_type_id { get; set; }
		public string course_name { get; set; }
		public string car_type { get; set; }
		public DateTime schedule { get; set; }
		public int price { get; set; }
		public string image {get; set; }
	}
}
