﻿namespace otomobil_backend.Models
{
    public class Course
    {
        public int id { get; set; }
        public string course_name { get; set; }
        public int fk_car_type_id { get; set; }
        public string desc { get; set; }
        public int price { get; set; }
        public string image { get; set; }

    }

    public class CourseWithNameType : Course
    {
        public string type { get; set; }
    }

    public class CourseInsert
    {
        public string course_name { get; set; }
        public int fk_car_type_id { get; set; }
        public string desc { get; set; }
        public int price { get; set; }
        public string image { get; set; }

    }

 
}
