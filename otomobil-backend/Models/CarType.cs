﻿namespace otomobil_backend.Models
{
    public class CarType
    {
        public int id { get; set; }
        public string name { get; set; }
        public string desc { get; set; }
        public string img { get; set; }
        public string thumbnail { get; set; }
    }

    public class CarTypeInsert
    {
        public string name { get; set; }
        public string desc { get; set; }
        public string img { get; set; }
        public string thumbnail { get; set; }
    }
}
