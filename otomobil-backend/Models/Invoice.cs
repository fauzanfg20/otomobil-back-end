﻿using System;

namespace otomobil_backend.Models
{
	public class Invoice
	{
		public int invoice_id { get; set; }
		public int fk_user_id { get; set; }
		public string user_name { get; set; }
		public string user_email { get; set; }
		public DateTime invoice_date { get; set; }
		public int total_price { get; set; }
		public string payment_method { get; set; }	
	}

	public class InvoiceWithDetail : Invoice {
		public List<DetailInvoice> course_list { get; set; }
    }
}
