﻿namespace otomobil_backend.Models
{
    public class Cart
    {
        public int id{ get; set; }
        public int car_id { get; set; }
        public string type { get; set; } 
        public string course_name { get; set; }
        public int price { get; set; }
        public string image { get; set; }
        public string schedule { get; set; }
        public int user_id { get; set; }


    }

    public class CartInsert
    {
        public int car_id { get; set; }
        public string type { get; set; }
        public string course_name { get; set; }
        public int price { get; set; }
        public string image { get; set; }
        public string schedule { get; set; }
        public int user_id { get; set; }
    }
}
