﻿namespace otomobil_backend.Models
{
    public class PaymentMethod
    {
        public int id { get; set; }
        public string name { get; set; }
        public string icon { get; set; }
        public bool active { get; set; }
    }

    public class PaymentMethodInsert
    {
        public string name { get; set; }
        public string icon { get; set; }
        public bool active { get; set; }
    }
    public class PaymentMethodPatch
    {
        public bool active { get; set; }
    }

}
