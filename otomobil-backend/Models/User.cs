﻿namespace otomobil_backend.Models
{
    public class UserRequest
    {
        public string? email { get; set; }
        public string? password { get; set; }
    }
    public class User
    {
        public int? user_id { get; set; }
        public string? name { get; set; }
        public string? email { get; set; }
        public string? password { get; set; }
        
    }

    public class RegisterUser
    {
        public string? name { get; set; }
        public string? email { get; set; }
        public string? password { get; set; }
    }

    public class UserWithAuth : User 
    {
        public bool? confirmed_email { get; set; }
        public string? password_hash { get; set; }
        public string? salt_hash { get; set; }
        public bool? can_be_admin { get; set; }
        public string? role { get; set; }
    }

    public class Token
    {
        public string? token_id { get; set; }
        public string? fk_user_id { get; set; }
        public string? otp { get; set; }
        public string? register_token { get; set; }
        public string? reset_token { get; set; }
        public string? expired_date { get; set; }
        public string? email { get; set; }

    }
    public class UserOuput
    {
        public int? user_id { get; set; }
        public string? email { get; set; }
        public string? role { get; set; }
        public string? jwt_token { get; set; }
    }

    public class UserPasswordReset
    {
        public string? password { get; set; }
    }

    public class UserResendEmail
    {
        public string? register_token { get; set; }
        public string? otp { get; set; }
        public string? reset_token { get; set; }
    }

    public class UserAdd
    {
        public string? name { get; set; }
        public string? email { get; set; }
        public string? password { get; set; }
        public bool? confirmed_email { get; set; }
        public string? role { get; set; }

    }
    public class UserAddBody
    {
        public string? name { get; set; }
        public string? email { get; set; }
        public string? password { get; set; }
        public bool? confirmed_email { get; set; }
        public string? role { get; set; }

    }

}
